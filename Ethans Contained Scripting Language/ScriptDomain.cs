﻿//Ethan Alexander Shulman 2016
using System;
using System.IO;
using EthansContainedScriptingLanguageDomain;

namespace EthansContainedScriptingLanguage
{
    public class ScriptDomain
    {

        private AppDomain appDomain;
        private Domain domain;

        public ScriptDomain()
        {
            AppDomainSetup domainSetup = new AppDomainSetup()
            {
                ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase,
                ConfigurationFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile,
                ApplicationName = AppDomain.CurrentDomain.SetupInformation.ApplicationName,
                LoaderOptimization = LoaderOptimization.MultiDomainHost
            };

            System.Reflection.Assembly[] assems = AppDomain.CurrentDomain.GetAssemblies();
            string[] names = new string[assems.Length];
            for (int i = 0; i < assems.Length; i++)
            {
                names[i] = assems[i].Location;
            }

            appDomain = AppDomain.CreateDomain("ScriptDomain", null, domainSetup);
            domain = (Domain)appDomain.CreateInstanceFromAndUnwrap("Ethans Contained Scripting Language Domain.dll", "EthansContainedScriptingLanguageDomain.Domain", false, System.Reflection.BindingFlags.Default, null, new object[]{names}, null, null, null);
            domain.SetOutput(Console.Out);
        }

        public void SetOutput(TextWriter o)
        {
            domain.SetOutput(o);
        }

        public Script Compile(string name, string src)
        {
            return domain.Compile(name, src);
        }

        public void Unload()
        {
            AppDomain.Unload(appDomain);
        }

    }
}
