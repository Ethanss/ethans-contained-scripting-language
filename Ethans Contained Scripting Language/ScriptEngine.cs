﻿//Ethan Alexander Shulman 2016
using System;
using System.Collections.Generic;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using System.Reflection;
using EthansContainedScriptingLanguageDomain;


namespace EthansContainedScriptingLanguage
{
    public class ScriptEngine
    {
        public const string RUNTIME_NAMESPACE = "ECSLRNS";


        private List<string> assemblyReferences = new List<string>(),
                             namespaceReferences = new List<string>(),
                             classReferences = new List<string>();
        private string usingNamespaces = "";

        private TextWriter output;


        public ScriptEngine()
        {
            output = Console.Out;
        }

        public string Prepare(string scriptName, string src)
        {
            //check for not allowed code
            string sum = "";
            bool inQuote = false, inDQuote = false;
            for (int i = 0; i < src.Length; i++)
            {
                char c = src[i];
                if (c == '\'')
                {
                    if (!inDQuote) inQuote = !inQuote;
                }
                else if (c == '"')
                {
                    if (!inQuote) inDQuote = !inDQuote;
                }
                else if (!inQuote && !inDQuote)
                {
                    if (c == '.')
                    {
                        if (i + 1 <= src.Length && !Char.IsLetter(src[i + 1])) continue;//skip decimal number

                        int len = 0;
                        for (int k = 1; k < 128; k++)
                        {
                            int ind = i - k;
                            if (ind < 0) break;
                            char c2 = src[ind];
                            if (!Char.IsLetter(c2)) break;
                            len++;
                        }
                        string refName = src.Substring(i - len, len);
                        bool validRefName = false;
                        if (refName == "Math")
                        {
                            validRefName = true;
                        }
                        else
                        {

                            //check if valid reference
                            for (int k = 0; k < classReferences.Count; k++)
                            {
                                if (refName == classReferences[k])
                                {
                                    validRefName = true;
                                    break;
                                }
                            }

                            //check if local variable
                            if (src.Contains(refName + " = "))
                            {
                                validRefName = true;
                            }
                        }
                        if (!validRefName)
                        {
                            //class reference not allowed
                            output.WriteLine("Error compiling " + scriptName + " at section \n'" + src.Substring(Math.Max(0, i - 10), Math.Min(20, src.Length - (i - 10))) + "'\n. Class or namespace reference "+refName+" not allowed.");
                            return null;
                        }
                    }
                    else
                    {
                        if (sum.Length == 0)
                        {
                            if (c == 'n')
                            {
                                sum += 'n';
                            }
                        }
                        else if (sum.Length == 1)
                        {
                            if (c == 'e')
                            {
                                sum += 'e';
                            }
                            else
                            {
                                sum = "";
                            }
                        }
                        else if (sum.Length == 2)
                        {
                            if (c == 'w')
                            {
                                sum += 'w';
                            }
                            else
                            {
                                sum = "";
                            }
                        }
                        else
                        {
                            if (c == ' ')
                            {
                                //new command, check if allowed class
                                int len = 0;
                                for (int k = 1; k < 128; k++)
                                {
                                    int ind = i + k;
                                    if (ind >= src.Length) break;
                                    char c2 = src[ind];
                                    if (!Char.IsLetter(c2)) break;
                                    len++;
                                }
                                bool validRefName = false;
                                string refName = src.Substring(i + 1, len);
                                //check if valid reference
                                for (int k = 0; k < classReferences.Count; k++)
                                {
                                    if (refName == classReferences[k])
                                    {
                                        validRefName = true;
                                        break;
                                    }
                                }
                                if (!validRefName)
                                {
                                    //class reference not allowed
                                    output.WriteLine("Error compiling " + scriptName + " at section \n'" + src.Substring(Math.Max(0, i - 10), Math.Min(20, src.Length - (i - 10))) + "'\n. Class or namespace reference "+refName+" not allowed.");
                                    return null;
                                }
                            }
                            sum = "";
                        }
                    }
                }
            }

            //prepare source code
            return usingNamespaces + " namespace " + RUNTIME_NAMESPACE + " {public class " + scriptName + " {" + src.Replace("Math.", "System.Math.") + "}}";
        }
        public Script Compile(ScriptDomain domain, string scriptName, string src)
        {
            //compile with script domain
            string psrc = Prepare(scriptName, src);
            if (psrc == null) return null;
            return domain.Compile(scriptName, psrc);
        }


        //add allowed class
        public void AllowClass(string name)
        {
            classReferences.Add(name);
        }
        public void DisallowClass(string name)
        {
            classReferences.Remove(name);
        }

        //add using namespace
        public void AddNamespace(string ns)
        {
            namespaceReferences.Add(ns);
            usingNamespaces += "using " + ns + ";\n ";
        }
        //remove using namespace
        public void RemoveNamespace(string ns)
        {
            namespaceReferences.Remove(ns);
            usingNamespaces = "";
            for (int i = 0; i < namespaceReferences.Count; i++)
            {
                usingNamespaces += namespaceReferences[i];
            }
        }


        //set script output writer
        public void SetOutput(TextWriter tw)
        {
            output = tw;
        }
    }
}
