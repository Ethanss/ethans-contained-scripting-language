﻿//Ethan Alexander Shulman 2016
using System;
using System.Collections.Generic;

namespace EthansContainedScriptingLanguageDomain
{
    public delegate T Func<T>(T t);

    public class ScriptInstance : MarshalByRefObject
    {
        private object instance;
        private Dictionary<string, Func<object>> functions;

        public ScriptInstance(object obj, Dictionary<string, Func<object>> funcs)
        {
            instance = obj;
            functions = funcs;
        }

        //execute script functions synchronous and asynchronous
        public object Invoke(string functionName, object arguments)
        {
            return functions[functionName].Invoke(arguments);
        }
        public void InvokeAsync(string functionName, object arguments)
        {
            functions[functionName].BeginInvoke(arguments, null, null);
        }
        public void InvokeAsync(string functionName, object arguments, AsyncCallback callback)
        {
            functions[functionName].BeginInvoke(arguments, callback, null);
        }


        public bool ContainsMethod(string functionName)
        {
            return functions.ContainsKey(functionName);
        }
    }
}
