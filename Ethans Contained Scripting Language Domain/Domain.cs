//Ethan Alexander Shulman 2016
using System;
using System.IO;
using System.CodeDom.Compiler;

namespace EthansContainedScriptingLanguageDomain
{
    public class Domain : MarshalByRefObject
    {

        private TextWriter output;
        private CodeDomProvider codeDomProvider;
        private CompilerParameters compileParams;

        public Domain()
        {
            System.Reflection.Assembly[] assems = AppDomain.CurrentDomain.GetAssemblies();
            string[] names = new string[assems.Length];
            for (int i = 0; i < assems.Length; i++)
            {
                names[i] = assems[i].Location;
            }

            codeDomProvider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("C#");
            compileParams = new CompilerParameters(names);
            compileParams.GenerateInMemory = true;
            compileParams.IncludeDebugInformation = false;
        }

        public Domain(string[] assemblies)
        {
            codeDomProvider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("C#");
            compileParams = new CompilerParameters(assemblies);
            compileParams.GenerateInMemory = true;
            compileParams.IncludeDebugInformation = false;
        }


        public void SetOutput(TextWriter o)
        {
            Console.SetOut(o);
            output = o;
        }

        public Script Compile(string name, string code)
        {
            try
            {
                CompilerResults results = codeDomProvider.CompileAssemblyFromSource(compileParams, new string[] { code });
                for (int i = 0; i < results.Errors.Count; i++)
                {
                    output.WriteLine(results.Errors[i].ErrorText.Replace("ECSLRNS.", ""));
                }

                if (results.CompiledAssembly == null) return null;
                return new Script(name, results.CompiledAssembly);
            }
            catch (Exception) { }

            return null;
        }
    }
}
