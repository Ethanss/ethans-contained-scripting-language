﻿//Ethan Alexander Shulman 2016
using System;
using System.Collections.Generic;
using System.Reflection;



namespace EthansContainedScriptingLanguageDomain
{

    public class Script : MarshalByRefObject
    {
        public const string RUNTIME_NAMESPACE = "ECSLRNS";


        //class name for script
        public string name;
        private Assembly codeAssembly;
        private List<MethodInfo> functions;


        public Script(string nm, Assembly cas)
        {
            name = nm;
            codeAssembly = cas;

            Type type = codeAssembly.GetType(RUNTIME_NAMESPACE + "." + name);
            MethodInfo[] methods = type.GetMethods();
            List<MethodInfo> publicMethods = new List<MethodInfo>();
            for (int i = 0; i < methods.Length; i++)
            {
                if (methods[i].IsPublic && !methods[i].IsConstructor) publicMethods.Add(methods[i]);
            }
            functions = publicMethods;
        }

        //create new instance of script class object
        public ScriptInstance NewInstance()
        {
            object inst = codeAssembly.CreateInstance(RUNTIME_NAMESPACE + "." + name); 

            //create delegates linked to instance
            Dictionary<string, Func<object>> funcDict = new Dictionary<string, Func<object>>();
            for (int i = 0; i < functions.Count; i++)
            {
                MethodInfo mi = functions[i];
                try
                {
                    Func<object> dg = (Func<object>)Delegate.CreateDelegate(typeof(Func<object>), inst, mi);
                    funcDict.Add(mi.Name, dg);
                }
                catch (ArgumentException) { }
            }

            return new ScriptInstance(inst, funcDict);
        }
    }
}
