Ethans Contained Scripting Language is a C# library to help with compiling and
running sandboxed C# code at run-time. It relies on CodeDomProvider and uses .NET 2.0
so it is compatible with the game engine Unity3D.


For more information along with tutorials check out the wiki https://bitbucket.org/Ethanss/ethans-contained-scripting-language/wiki/Home.